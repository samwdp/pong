package logic

import g "../game"
import "vendor:sdl2"
import "core:math/linalg"

update_entity :: proc(entity: ^g.Entity, game: ^g.Game, scene: ^g.Scene) {
    dt := f32(game.dt)
    switch entity.type {
    case .PLAYER:
        dir := [2]f32{0, 0}
        if b8(game.keyboard[sdl2.SCANCODE_W]) do dir.y -= 1
        if b8(game.keyboard[sdl2.SCANCODE_S]) do dir.y += 1
        dir = linalg.normalize0(dir)
        entity.pos += dir * 0.5 * dt
        entity.pos += entity.vel * dt
        entity.vel -= entity.vel * 0.9999 / dt
        // Keep it in the map
        entity.pos.y = clamp(entity.pos.y, 20, cast(f32)g.WINDOW_HEIGHT - 20)
    case .AIPLAYER:
        dir := [2]f32{0, 0}
        if game.multiplayer {
            if b8(game.keyboard[sdl2.SCANCODE_UP]) do dir.y -= 1
            if b8(game.keyboard[sdl2.SCANCODE_DOWN]) do dir.y += 1
            dir = linalg.normalize0(dir)
            entity.pos += dir * 0.5 * dt
            entity.pos += entity.vel * dt
            entity.vel -= entity.vel * 0.9999 / dt

        }
        entity.pos.y = clamp(entity.pos.y, 20, cast(f32)g.WINDOW_HEIGHT - 20)
    case .BALL:
        //check collision
        dir := [2]f32{0, 0}
        /* dir.y -= 1 */

        entity.pos.x += entity.vel.x * dt
        entity.pos.y += entity.vel.y * dt
        player := g.find_entity(.PLAYER, scene)
        ai := g.find_entity(.AIPLAYER, scene)

        if entity.pos.x < 0 {
            entity.vel.x = abs(entity.vel.x)
        }

        if entity.pos.x > f32(g.WINDOW_WIDTH) - entity.dim.x {
            entity.vel.x = -abs(entity.vel.x)
        }

        if entity.pos.y < 0 {
            entity.vel.y = abs(entity.vel.y)
        }

        if entity.pos.y > f32(g.WINDOW_HEIGHT) - entity.dim.x {
            entity.vel.y = -abs(entity.vel.y)
        }

        if entity.pos.y < ai.pos.y + ai.dim.y / 2 &&
           entity.pos.y > ai.pos.y - ai.pos.y / 2 &&
           entity.pos.x + entity.dim.x < ai.pos.x - ai.dim.x / 2 {
            entity.vel.x = -abs(entity.vel.x)
            entity.vel.y = -abs(entity.vel.y)
        }


        //check collision
        if entity.pos.y < player.pos.y + player.dim.y / 2 &&
           entity.pos.y > player.pos.y - player.pos.y / 2 &&
           entity.pos.x - entity.dim.x < player.pos.x + player.dim.x / 2 {
            entity.vel.x = abs(entity.vel.x)
            entity.vel.y = abs(entity.vel.y)
        }


    /* if entity.pos.x < player.pos.x + player.dim.x { */
    /*     entity.vel.x = -abs(entity.vel.x) */
    /* } */


    /* if entity.pos.x >= ai.pos.x - 20 && entity.vel.x > 0 { */
    /*     /\* if entity.pos.y < ai.pos.y - 10 && entity.vel.y > 0 { *\/ */
    /*     entity.pos.x += dt */
    /*     log.info("Hit ai") */
    /*     /\* } *\/ */
    /* } */
    /* if entity.pos.x < player.pos.x + 10 && entity.vel.x < 0 { */
    /*     entity.pos.x -= dt */
    /*     log.info("Hit player") */
    /* } */
    /* entity.pos.y = clamp(entity.pos.y, 0, f32(WINDOW_HEIGHT - 10)) */
    /* entity.pos.x = clamp(entity.pos.x, 0, f32(WINDOW_WIDTH - 10)) */

    case .BUTTON:
        entity.action(game)
    }
}
