package render

import "core:fmt"
import "vendor:sdl2"
import "vendor:sdl2/ttf"
import "core:c"
import "core:strings"
import g "../game"

render_text_f :: proc(
    renderer: ^sdl2.Renderer,
    font: ^ttf.Font,
    value: cstring,
    x,
    y,
    w,
    h: f32,
) {
    surface := ttf.RenderText_Solid(font, value, {255, 0, 0, 0})
    defer sdl2.FreeSurface(surface)
    tex := sdl2.CreateTextureFromSurface(renderer, surface)
    defer sdl2.DestroyTexture(tex)
    r := sdl2.FRect {
        x = x,
        y = y,
        w = w,
        h = h,
    }

    sdl2.RenderCopyF(renderer, tex, nil, &r)
}

render_draw_circle_f :: proc(renderer: ^sdl2.Renderer, x, y, r: f32) -> c.int {
    offsetx, offsety, d: f32
    status: c.int

    offsetx = 0
    offsety = r
    d = r - 1
    status = 0


    for offsety >= offsetx {
        status += sdl2.RenderDrawLineF(
            renderer,
            x - offsety,
            y + offsetx,
            x + offsety,
            y + offsetx,
        )
        status += sdl2.RenderDrawLineF(
            renderer,
            x - offsetx,
            y + offsety,
            x + offsetx,
            y + offsety,
        )
        status += sdl2.RenderDrawLineF(
            renderer,
            x - offsetx,
            y - offsety,
            x + offsetx,
            y - offsety,
        )
        status += sdl2.RenderDrawLineF(
            renderer,
            x - offsety,
            y - offsetx,
            x + offsety,
            y - offsetx,
        )

        if status < 0 {
            status = -1
            break
        }

        if d >= 2 * offsetx {
            d -= 2 * offsetx + 1
            offsetx += 1
        } else if d < 2 * (r - offsety) {
            d += 2 * offsety - 1
            offsety -= 1
        } else {
            d += 2 * (offsety - offsetx - 1)
            offsety -= 1
            offsetx += 1
        }
    }
    return status
}

draw_button_f :: proc(
    renderer: ^sdl2.Renderer,
    font: ^ttf.Font,
    x,
    y,
    w,
    h: f32,
    text: string,
) {
    /* sdl2.RenderDrawRectF(renderer, &sdl2.FRect{x = x, y = y, w = w, h = h}) */
    r := sdl2.FRect {
        x = x,
        y = y,
        w = w,
        h = h,
    }


    sdl2.RenderDrawRectF(renderer, &r)

    render_text_f(renderer, font, strings.clone_to_cstring(text), x, y, w, h)
}


render_entity :: proc(game: ^g.Game, entity: ^g.Entity) {
    switch entity.type {
    case .PLAYER:
        sdl2.SetRenderDrawColor(game.renderer, 255, 0, 255, 0)
        sdl2.RenderDrawRectF(
            game.renderer,
            &sdl2.FRect{
                x = entity.pos.x,
                y = entity.pos.y - entity.dim.y / 2,
                w = entity.dim.x,
                h = entity.dim.y,
            },
        )
    case .AIPLAYER:
        sdl2.SetRenderDrawColor(game.renderer, 255, 255, 0, 0)
        sdl2.RenderDrawRectF(
            game.renderer,
            &sdl2.FRect{
                x = entity.pos.x,
                y = entity.pos.y - entity.dim.y / 2,
                w = entity.dim.x,
                h = entity.dim.y,
            },
        )

    case .BALL:
        sdl2.SetRenderDrawColor(game.renderer, 255, 0, 0, 0)
        render_draw_circle_f(
            game.renderer,
            entity.pos.x,
            entity.pos.y - entity.dim.x / 2,
            entity.dim.x,
        )
    case .BUTTON:
        draw_button_f(
            game.renderer,
            game.font,
            entity.pos.x,
            entity.pos.y,
            entity.dim.x,
            entity.dim.y,
            entity.desc,
        )
    }
}

render_entities :: proc(game: ^g.Game, scene: ^g.Scene) {
    for _, i in scene.entities {
        render_entity(game, &scene.entities[i])
    }
}

render_net :: proc(game: ^g.Game) {
    sdl2.SetRenderDrawColor(game.renderer, 255, 255, 255, 0)
    sdl2.RenderDrawLineF(
        renderer = game.renderer,
        x1 = cast(f32)g.WINDOW_WIDTH / 2,
        y1 = 0.0,
        x2 = cast(f32)g.WINDOW_WIDTH / 2,
        y2 = cast(f32)g.WINDOW_WIDTH,
    )
}

render_game :: proc(game: ^g.Game, scene: ^g.Scene) {
    render_net(game)
    buf := strings.Builder{}
    render_text_f(
        game.renderer,
        game.font,
        strings.clone_to_cstring(fmt.sbprintf(&buf, "%v", game.player1score)),
        cast(f32)g.WINDOW_WIDTH / 2 - 75,
        0,
        30,
        30,
    )
    buf = strings.Builder{}
    render_text_f(
        game.renderer,
        game.font,
        strings.clone_to_cstring(fmt.sbprintf(&buf, "%v", game.player2score)),
        cast(f32)g.WINDOW_WIDTH / 2 + 40,
        0,
        30,
        30,
    )
    render_entities(game, scene)
}

render_title :: proc(game: ^g.Game, scene: ^g.Scene) {
    sdl2.SetRenderDrawColor(game.renderer, 255, 255, 255, 0)
    render_entities(game, scene)
}

render_end :: proc(game: ^g.Game, scene: ^g.Scene) {
    sdl2.SetRenderDrawColor(game.renderer, 255, 255, 255, 0)
    render_text_f(
        game.renderer,
        game.font,
        strings.clone_to_cstring("Winner"),
        cast(f32)g.WINDOW_WIDTH / 2 - 75,
        0,
        300,
        200,
    )
    winner := game.player1score > game.player2score ? "Player 1" : "Player 2"
    render_text_f(
        game.renderer,
        game.font,
        strings.clone_to_cstring(winner),
        300,
        300,
        300,
        200,
    )

    render_entities(game, scene)
}
