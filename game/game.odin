package game

import "vendor:sdl2"
import "vendor:sdl2/ttf"

WINDOW_TITLE :: "Pong"
WINDOW_WIDTH: i32 = 1280
WINDOW_HEIGHT: i32 = 720
TICKRATE := 240.0
TICKTIME := 1000.0 / TICKRATE
WINNING_SCORE := 1000

EntityType :: enum {
    AIPLAYER,
    PLAYER,
    BALL,
    BUTTON,
}

Entity :: struct {
    id:     int,
    type:   EntityType,
    pos:    [2]f32,
    dim:    [2]f32,
    vel:    [2]f32,
    action: proc(game: ^Game),
    desc:   string,
}

Game :: struct {
    font:         ^ttf.Font,
    renderer:     ^sdl2.Renderer,
    scenes:       [dynamic]Scene,
    player1score: int,
    player2score: int,
    dt:           f64,
    time:         f64,
    keyboard:     []u8,
    mouse:        u32,
    multiplayer:  bool,
}

SceneType :: enum {
    TITLE,
    GAME,
    END,
}

Scene :: struct {
    type:     SceneType,
    isActive: bool,
    entities: [dynamic]Entity,
}

find_entity :: proc(type: EntityType, scene: ^Scene) -> ^Entity {
    for _, i in scene.entities {
        if scene.entities[i].type == type {
            return &scene.entities[i]
        }
    }
    return nil
}

find_scene :: proc(game: ^Game) -> ^Scene {
    for _, i in game.scenes {
        if game.scenes[i].isActive == true {
            return &game.scenes[i]
        }
    }
    return nil
}

find_scene_by_type :: proc(type: SceneType, game: ^Game) -> ^Scene {
    for _, i in game.scenes {
        if game.scenes[i].type == type {
            return &game.scenes[i]
        }
    }
    return nil
}

get_time :: proc() -> f64 {
    return(
        f64(sdl2.GetPerformanceCounter()) *
        1000 /
        f64(sdl2.GetPerformanceFrequency()) \
    )
}
