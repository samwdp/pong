package pong

import "core:log"
import "core:strings"
import "vendor:sdl2"
import "vendor:sdl2/ttf"
import "core:c"
import ren "render"
import g "game"
import "logic"
import "core:math/rand"

new_game :: proc(game: ^g.Game) {
    game.player1score = 0
    game.player2score = 0
    if b8(game.keyboard[sdl2.SCANCODE_S]) {
        game.multiplayer = false
        game_scene := g.find_scene_by_type(.GAME, game)
        active_scene := g.find_scene(game)
        active_scene.isActive = false
        game_scene.isActive = true
    }

    if b8(game.keyboard[sdl2.SCANCODE_M]) {
        game.multiplayer = true
        game_scene := g.find_scene_by_type(.GAME, game)
        active_scene := g.find_scene(game)
        active_scene.isActive = false
        game_scene.isActive = true
    }
}

main :: proc() {
    ttf.Init()
    assert(sdl2.Init(sdl2.INIT_VIDEO) == 0, sdl2.GetErrorString())
    defer sdl2.Quit()
    defer ttf.Quit()
    window := sdl2.CreateWindow(
        g.WINDOW_TITLE,
        sdl2.WINDOWPOS_CENTERED,
        sdl2.WINDOWPOS_CENTERED,
        g.WINDOW_WIDTH,
        g.WINDOW_HEIGHT,
        sdl2.WINDOW_SHOWN,
    )
    assert(window != nil, sdl2.GetErrorString())
    defer sdl2.DestroyWindow(window)
    font := ttf.OpenFont("fonts/NotoSans-Regular.ttf", 400)
    defer ttf.CloseFont(font)

    renderer := sdl2.CreateRenderer(window, -1, sdl2.RENDERER_ACCELERATED)
    assert(renderer != nil, sdl2.GetErrorString())
    defer sdl2.DestroyRenderer(renderer)

    game := g.Game {
        font         = font,
        renderer     = renderer,
        dt           = g.TICKTIME,
        scenes       = make([dynamic]g.Scene),
        time         = g.get_time(),
        player1score = 0,
        player2score = 0,
    }

    game_scene := g.Scene {
        type     = .GAME,
        isActive = false,
        entities = make([dynamic]g.Entity),
    }

    title_scene := g.Scene {
        type     = .TITLE,
        isActive = true,
        entities = make([dynamic]g.Entity),
    }

    end_scene := g.Scene {
        type     = .END,
        isActive = false,
        entities = make([dynamic]g.Entity),
    }

    // Title
    append(
        &title_scene.entities,
        g.Entity{
            type = .BUTTON,
            id = 1,
            pos = {f32(g.WINDOW_WIDTH / 2) - 200, 10.0},
            dim = {400.0, 100.0},
            action = new_game,
            desc = "Play Pong (s)",
        },
    )
    append(
        &title_scene.entities,
        g.Entity{
            type = .BUTTON,
            id = 1,
            pos = {f32(g.WINDOW_WIDTH / 2) - 200, 210.0},
            dim = {400.0, 100.0},
            action = new_game,
            desc = "Multi Player (m)",
        },
    )

    // Game
    append(
        &game_scene.entities,
        g.Entity{
            type = .PLAYER,
            id = 1,
            pos = {10.0, (f32)(g.WINDOW_HEIGHT / 2)},
            dim = {10, 40},
        },
    )
    append(
        &game_scene.entities,
        g.Entity{
            type = .AIPLAYER,
            id = 1,
            pos = {(f32)(g.WINDOW_WIDTH - 20.0), (f32)(g.WINDOW_HEIGHT / 2)},
            dim = {10, 40},
        },
    )
    append(
        &game_scene.entities,
        g.Entity{
            type = .BALL,
            id = 3,
            vel = {-0.2, 0},
            dim = {5, 0},
            pos = {(f32)(g.WINDOW_WIDTH / 2), (f32)(g.WINDOW_HEIGHT / 2)},
        },
    )

    // end
    append(
        &end_scene.entities,
        g.Entity{
            type = .BUTTON,
            id = 1,
            pos = {10.0, 10.0},
            dim = {400.0, 100.0},
            action = new_game,
            desc = "Replay Pong (p)",
        },
    )


    // Add scenes
    append(&game.scenes, game_scene)
    append(&game.scenes, title_scene)
    append(&game.scenes, end_scene)

    defer delete(title_scene.entities)
    defer delete(game_scene.entities)
    defer delete(end_scene.entities)

    dt := 0.0

    for {
        // Process Input
        event: sdl2.Event
        for sdl2.PollEvent(&event) {
            #partial switch event.type {
            case .QUIT:
                return
            case .KEYDOWN:
                if event.key.keysym.scancode == sdl2.SCANCODE_ESCAPE {
                    return
                }
            }
        }

        // Update
        time := g.get_time()
        dt += time - game.time

        game.keyboard = sdl2.GetKeyboardStateAsSlice()
        game.time = time
        active_scene := g.find_scene(&game)
        for dt >= g.TICKTIME {
            dt -= g.TICKTIME
            for _, i in active_scene.entities {
                logic.update_entity(
                    &active_scene.entities[i],
                    &game,
                    active_scene,
                )
            }
            if active_scene.type == .GAME {
                if game.player1score == g.WINNING_SCORE &&
                       game.player1score > game.player2score ||
                   game.player2score == g.WINNING_SCORE &&
                       game.player2score > game.player1score {
                    game_scene := g.find_scene_by_type(.GAME, &game)
                    game_scene.isActive = false
                    end_scene := g.find_scene_by_type(.END, &game)
                    end_scene.isActive = true
                }
            }
        }

        // Render
        sdl2.SetRenderDrawColor(game.renderer, 0, 0, 0, 0)
        sdl2.RenderClear(game.renderer)
        switch active_scene.type {
        case .GAME:
            ren.render_game(&game, active_scene)
        case .END:
            ren.render_end(&game, active_scene)
        case .TITLE:
            ren.render_title(&game, active_scene)
        }
        sdl2.RenderPresent(game.renderer)
    }
}
